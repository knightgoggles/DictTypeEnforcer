#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Setup file for dicttypeenforcer.

    This file was generated with PyScaffold 2.5.7, a tool that easily
    puts up a scaffold for your new Python project. Learn more under:
    http://pyscaffold.readthedocs.org/
"""

import sys
from setuptools import setup


# This is the old setup_package() from pyscaffold - when I figure out how to version things properly with it, I'll uncomment it!
def setup_package():
    needs_sphinx = {'build_sphinx', 'upload_docs'}.intersection(sys.argv)
    sphinx = ['sphinx'] if needs_sphinx else []
    setup(setup_requires=['six', 'pyscaffold>=2.5a0,<2.6a0'] + sphinx,
          use_pyscaffold=False,
          version='0.1.2',
          packages=['dicttypeenforcer'],
          url='https://gitlab.com/knightgoggles/DictTypeEnforcer')


if __name__ == "__main__":
    setup_package()
