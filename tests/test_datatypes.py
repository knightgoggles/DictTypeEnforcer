# TODO: writea specific test for the Int converter

import pytest
import datetime
from dicttypeenforcer.datatypes import BaseDataTypeConverter, GenericDataTypeConverter, StringDataTypeConverter, \
    DateDataTypeConverter, DateTimeDataTypeConverter, IntDataTypeConverter, get_field_to_type_converter_mapping


def test_basedatatypeconverter():
    with pytest.raises(NotImplementedError):
        a = BaseDataTypeConverter(0.1)


def test_generic_dataype_converter_no_conversion():
    a = GenericDataTypeConverter(0.1)
    test_result = a.parse(0.1)
    assert test_result == 0.1


def test_generic_dataype_converter_with_conversion():
    a = GenericDataTypeConverter(0.1)
    test_result = a.parse("0.1")
    assert test_result == 0.1


def test_string_converter_float_strip_decimals():
    a = StringDataTypeConverter("0", strip_decimal_places_from_floats=True)
    test_result = a.parse(0.1)
    # print(test_result, type(test_result))
    assert test_result == "0"


def test_date_converter_from_date_string():
    test_date = datetime.date(2017, 1, 1)
    test_string = str(test_date)
    a = DateDataTypeConverter(test_date)
    test_result = a.parse(test_string)
    assert test_result == test_date


def test_date_converter_from_datetime_object():
    test_date = datetime.date(2017, 1, 1)
    a = DateDataTypeConverter(test_date)
    test_result = a.parse(datetime.datetime(2017, 1, 1, 1, 1))
    assert test_result == test_date


def test_datetime_converter_from_date_string():
    test_datetime = datetime.datetime(2017, 1, 1)
    test_string = str(test_datetime)
    a = DateTimeDataTypeConverter(test_datetime)
    test_result = a.parse(test_string)
    assert test_result == test_datetime


def test_correct_value_to_type_mappings():
    example_dict = {"a": "foobar", "b": 1, "c": datetime.datetime.now().date(),
                    "d": datetime.datetime.now(), "e": 0.01}
    test_result = get_field_to_type_converter_mapping(example_dict)
    assert isinstance(test_result["a"], StringDataTypeConverter)
    assert isinstance(test_result["b"], IntDataTypeConverter)
    assert isinstance(test_result["c"], DateDataTypeConverter)
    assert isinstance(test_result["d"], DateTimeDataTypeConverter)
    assert isinstance(test_result["e"], GenericDataTypeConverter)
