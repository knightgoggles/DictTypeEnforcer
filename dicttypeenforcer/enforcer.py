from dicttypeenforcer.datatypes import get_field_to_type_converter_mapping


class Enforcer(object):
    def __init__(self, example_dict):
        """
        A class to coerce data into the types that you need. Rather than specifying those types by hand, Enforcer allows
        you to provide an example dictionary that represents the output you want
        :param example_dict: dictionary example of the keys and types you need
        """
        if isinstance(example_dict, dict):
            self.example_dict = example_dict
        else:
            raise TypeError('example_dict argument must be a dict!')
        self.fields_and_types = self.get_key_type_map(example_dict)
        self.types_and_fields = self.get_type_to_key_map(example_dict)
        self.field_to_type_converter_mapping = get_field_to_type_converter_mapping(self.example_dict)

    def parse(self, input_dict):
        """
        Takes a dictionary as input, and coerces its datatypes to match those specified when Enforcer was instantiated
        :param input_dict: your raw dictionary
        :return: a corrected dictionary
        """
        return_dict = {}
        for key, value in input_dict.items():
            converter = self.field_to_type_converter_mapping.get(key)
            if converter is not None:
                converted_value = converter.parse(value)
                return_dict[key] = converted_value
        return return_dict

    def get_value_type(self, value):
        """Returns the string decription of an object's type"""
        return str(type(value))

    def get_key_type_map(self, input_dict):
        """Takes a dict as input, returns a dict with the keys of the original dict, but with the string representation
        of the original dicts values"""
        return_dict = {}
        for key, value in input_dict.items():
            return_dict[key] = self.get_value_type(value)
        return return_dict

    def get_type_to_key_map(self, input_dict):
        """Takes a dict as input, returns a dict with string type representations as keys, and lists of the keys of all
        matching types of the original dict"""
        return_dict = {}
        for key, value in input_dict.items():
            value_type = self.get_value_type(value)
            if return_dict.get(value_type) is None:
                return_dict[value_type] = [key]
            else:
                return_dict[value_type].append(key)
        return return_dict
