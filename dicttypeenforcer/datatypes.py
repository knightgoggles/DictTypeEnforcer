"""
Individual classes for each datatype as in some cases very specific logic is required
"""
# TODO: handle case where the value is a dictionary, need a recursive solution

import datetime


def get_field_to_type_converter_mapping(example_dict):
    mapping = {}
    for key, value in example_dict.items():
        if isinstance(value, str):
            mapping[key] = StringDataTypeConverter(value)
        elif isinstance(value, int):
            mapping[key] = IntDataTypeConverter(value)
        elif value.__class__ is datetime.date:  # TODO: this is a nasty typecheck, need to find why "isinstance(value, datetime.date)" is not suitable
            mapping[key] = DateDataTypeConverter(value)
        elif isinstance(value, datetime.datetime):
            mapping[key] = DateTimeDataTypeConverter(value)
        else:
            mapping[key] = GenericDataTypeConverter(value)
    return mapping


class BaseDataTypeConverter(object):
    def __init__(self, value):
        raise NotImplementedError

    def parse(self, value):
        raise NotImplementedError


class GenericDataTypeConverter(BaseDataTypeConverter):
    def __init__(self, value):
        self.datatype = type(value)

    def parse(self, value):
        if isinstance(value, self.datatype):
            return value
        else:
            return self.datatype(value)


class StringDataTypeConverter(BaseDataTypeConverter):
    def __init__(self, value, strip_decimal_places_from_floats=True):
        if isinstance(value, str):
            self.datatype = type(value)
        else:
            raise ValueError("StringDataTypeConverter can only be initialised with a string!")
        self.strip_decimals = strip_decimal_places_from_floats

    def parse(self, value):
        if isinstance(value, self.datatype):
            return value
        elif isinstance(value, float) and self.strip_decimals is True:
            return str(int(value))
        else:
            return str(value)


class IntDataTypeConverter(BaseDataTypeConverter):
    def __init__(self, value):
        if isinstance(value, int):
            self.datatype = type(value)
        else:
            raise ValueError("IntDataTypeConverter can only be initialised with an int!")

    def parse(self, value):
        if isinstance(value, self.datatype):
            return value
        elif isinstance(value, str):
            return int(float(value))
        else:
            return int(value)


class DateDataTypeConverter(BaseDataTypeConverter):
    def __init__(self, value, format_string='%Y-%m-%d'):
        if value.__class__ is datetime.date:
            self.datatype = type(value)
        else:
            raise ValueError("DateDataTypeConverter can only be initialised with an date!")
        self.format_string = format_string

    def parse(self, value):
        if value.__class__ is datetime.datetime:
            return value.date()
        elif value.__class__ is datetime.date:
            return value
        else:
            if isinstance(value, str):
                return datetime.datetime.strptime(value[0:10], self.format_string).date()


class DateTimeDataTypeConverter(BaseDataTypeConverter):
    def __init__(self, value, format_string='%Y-%m-%d  %H:%M:%S'):
        if isinstance(value, datetime.datetime):
            self.datatype = type(value)
        else:
            raise ValueError("DateTimeDataTypeConverter can only be initialised with a datetime!")
        self.format_string = format_string

    def parse(self, value):
        if isinstance(value, self.datatype):
            return value
        else:
            if isinstance(value, str):
                return datetime.datetime.strptime(value, self.format_string)
