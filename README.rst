================
DictTypeEnforcer
================


A tool to help handle dirty input data by specifying an example of what you need the data to look like.


Description
===========

Are you an analyst who deals with mistyped data on a daily basis? Well never fear, because DictTypeEnforcer is here!

Install from PyPI thusly: pip install dicttypeenforcer

Just instantiate the Enforcer class with an example dictionary, e.g.

>>> from dicttypeenforcer import Enforcer

>>> enforcer = Enforcer({"foo":0.1, "bar":"0"})

This will specify what you're after. Now if you call the parse() method on enforcer with another dictionary, e.g.:

>>> enforcer.parse({"foo":"0.1", "bar":"0.1"}

You should get the types as specified!

Types currently handled:
    int, float, str, date, datetime.

Any other types will be given a "best guess" but YMMV.

TODOs:
    Add recursive handling of dictionaries as values


Note
====

This project has been set up using PyScaffold 2.5.7. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
