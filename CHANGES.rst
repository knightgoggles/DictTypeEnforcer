=========
Changelog
=========

Version 0.1
===========

- Initial release version 0.1

Version 0.1.1
===========

- Add more detail to README.rst

Version 0.1.2
===========

- fix gitlab issue #1 (datetime not being converted to date)
