# -*- coding: utf-8 -*-
import pkg_resources

try:
    __version__ = pkg_resources.get_distribution(__name__).version
except:
    __version__ = 'unknown'

__version__ = "0.1.2"

__author__ = "Peter Knight"
__copyright__ = "Peter Knight"
__license__ = "MIT"

from dicttypeenforcer.enforcer import Enforcer
