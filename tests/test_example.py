#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from dicttypeenforcer.enforcer import Enforcer

example_dict = {"a": 0, "b": 0.1, "c": "0"}


def test_init_with_dict():
    a = Enforcer(example_dict)
    assert isinstance(a.example_dict, dict)
    with pytest.raises(TypeError):
        b = Enforcer([example_dict])


def test_main_approach():
    a = Enforcer(example_dict)
    test_result = a.parse({"a": "0.1", "b": "0.1", "c": 0.1})
    assert test_result.get("a") == example_dict.get("a")
    assert test_result.get("b") == example_dict.get("b")
    assert test_result.get("c") == example_dict.get("c")


def test_type_detectionfor_dictionary():
    a = Enforcer(example_dict)
    for key, value in example_dict.items():
        print(a.get_value_type(value))
    test_result = a.get_key_type_map(example_dict)
    assert test_result.get("a") == "<class 'int'>"
    assert test_result.get("b") == "<class 'float'>"
    assert test_result.get("c") == "<class 'str'>"

    test_result = a.get_type_to_key_map(example_dict)
    assert test_result.get("<class 'int'>") == ["a"]


def test_nested_dict_handling():
    example = {"a": "foo", "b": {"c": 0.1}}
    a = Enforcer(example)
    test_result = a.parse({"a": "foo", "b": {"c": "0.1"}})
    first_level = test_result.get("b")
    assert first_level.get("c") == 0.1
